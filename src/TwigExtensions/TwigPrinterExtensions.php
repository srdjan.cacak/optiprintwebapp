<?php

namespace App\TwigExtensions;

use App\Entity\Printers;
use App\Managers\PrinterCounterManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigPrinterExtensions extends AbstractExtension
{

    private EntityManagerInterface $entityManager;
    private PrinterCounterManager $counterManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        PrinterCounterManager $counterManager
    ) {

        $this->entityManager = $entityManager;
        $this->counterManager = $counterManager;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_present_counter', [$this, 'presentCounter']),
        ];
    }

    /**
     * @param int|null $printerID
     * @return int
     * @throws Exception
     */
    public function presentCounter(?int $printerID): int
    {
        if(!$printerID)
            return 0;

        $printer = $this->entityManager->getRepository(Printers::class)->find($printerID);

        if(!$printer)
            return 0;

        return $this->counterManager->mapCountersForMonth($printer)['presentMonth'] ?? 0;
    }
}