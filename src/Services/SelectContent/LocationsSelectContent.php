<?php

namespace App\Services\SelectContent;

use App\Interfaces\SelectInterface;
use App\Traits\SelectTrait;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\ContactBundle\Entity\AccountAddress;


class LocationsSelectContent implements SelectInterface
{

    use SelectTrait;

    private EntityManagerInterface $em;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @return array
     */
    public function getValues(): array {
        $valueList = [];

        foreach($this->getAllRows() as $entity)
        {

            /**
             * @var AccountAddress $entity
             */
            $valueList[] = [
                'name' => $entity->getId(),
                'title' => '#' . $entity->getId() . ': ' . $entity->getAccount()->getName() . ' - ' . $entity->getAddress()->getTitle()
            ];
        }

        return $valueList;
    }

    /**
     * @param bool $multiSelect
     * @return int|array
     */
    public function getDefaultValue(?bool $multiSelect = false) {
        $values = $this->getValues();

        if(!$values)
            return -1;

       return $multiSelect ? [$this->getValues()] : $this->getValues()[0]['name'];
    }


    /**
     * @return array
     */
    public function getUnassigned(): array {
        
        $valueList = [];

        foreach($this->getAllRows() as $entity)
        {
            /**
             * @var AccountAddress $entity
             */
            $valueList[] = [
                    'name' => $entity->getId(),
                    'title' => $entity->getAddress()->getTitle()
            ];
        }

        return $valueList;
    }


    /**
     * @return array|object[]
     */
    public function getAllRows(): array
    {
        return $this->em->getRepository(AccountAddress::class)->findAll();
    }
}