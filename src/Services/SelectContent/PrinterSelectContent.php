<?php

namespace App\Services\SelectContent;

use App\Repository\PrintersRepository;
use App\Interfaces\SelectInterface;
use App\Traits\SelectTrait;


class PrinterSelectContent implements SelectInterface
{

    use SelectTrait;

    /**
     * @param PrintersRepository $entityRepository
     */
    public function __construct(PrintersRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }


    /**
     * @return array
     */
    public function getUnassigned(): array {
        
        $valueList = [];

        foreach($this->entityRepository->findAll() as $entity)
        {
            if(!$entity->getOrganizationLocations())
                $valueList[] = [
                    'name' => $entity->getId(),
                    'title' => $entity->getName()
                ];
        }

        return $valueList;
    }
}