<?php

namespace App\Services\SelectContent;

use App\Repository\PrinterTypesRepository;
use App\Interfaces\SelectInterface;
use App\Traits\SelectTrait;


class PrinterTypeSelectContent implements SelectInterface
{

    use SelectTrait;

    /**
     * @param PrinterTypesRepository $entityRepository
     */
    public function __construct(PrinterTypesRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
        $this->valueAttribute = 'typeName';
    }

}