<?php

namespace App\Traits;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

trait SelectTrait
{

    /**
     * @var ServiceEntityRepository $valueRepository;
     */
    private $entityRepository;

    private string $nameAttribute = 'id';
    private string $valueAttribute = 'name';

    /**
     * @return array
     */
    public function getValues(): array {
        $valueList = [];

        $name = "get". ucfirst($this->nameAttribute);
        $value = "get".ucfirst($this->valueAttribute);

        if(!$this->nameAttribute || !$this->valueAttribute || !$this->entityRepository)
            return $valueList;

        foreach($this->entityRepository->findAll() as $entity)
        {
            $valueList[] = [
                'name' => $entity->$name(),
                'title' => $entity->$value()
            ];
        }

        return $valueList;
    }

    /**
     * @param bool $multiSelect
     * @return int|array
     */
    public function getDefaultValue(?bool $multiSelect = false) {

        if(
            !$this->nameAttribute || 
            !$this->valueAttribute || 
            !count($this->getValues())
          )
            return false;

        if($this->getValues() == [])
            return 0;

        return $multiSelect ? [$this->getValues()] : $this->getValues()[0]['name'];
    }

}



