<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class EndpointController extends AbstractController
{
    
    public function index(Request $request): Response
    {
        $requestData = $this->mapData($request->request->all());

        if(!$requestData->isAllowed)
            throw new AccessDeniedHttpException();

        if(is_array($requestData->printerData))
            $this->updateCounters($requestData);

        return $this->json(
            [
                'partnerID' => $requestData->partnerID,
                'controlHashCheck' => $requestData->isAllowed,
                'hasher' => date('Ymd') . '-' . $requestData->partnerID,
                'checkHash' => md5(date('Ymd') . '-' . $requestData->parnerID),
                'printerData' => $requestData->printerData
            ]
        );
    }

    private function mapData(array $request)
    {
        $mapData['partnerID'] = $request['partnerid'];
        $mapData['isAllowed'] = (md5(date('Ymd') . '-' . $request['partnerid']) == $request['control-hash']) ?? false;
        $mapData['printerData'] = $request['submitData'] ?? 'No submit data!';

        return (object)$mapData;
    }

    private function updateCounters(array $requestData)
    {
        foreach($requestData->printerData as $id => $counter)
        {
            $prnCounter = new PrinterCounters();

            if($p = $this->countersRepository->findBy(['printer'=>$id, 'date'=>new \DateTime(), 'organization' => $requestData->partnerID]))
                $prnCounter = $p[0];

            if(!$p) {
                $printerObject = $this->repository->find($id);
                if($printerObject->getOrganizationLocations()->getAccount()->getId() != $requestData->partnerID)
                    continue;
                $prnCounter->setPrinter($printerObject);
                $prnCounter->setDate(new \DateTime());
                $prnCounter->setSerialNumber($printerObject->getSerialNumber());
                $prnCounter->setOrganization($printerObject->getOrganizationLocations()->getAccount());
            }

            $prnCounter->setDatetime(new \DateTime());
            $prnCounter->setCounter($counter);
            $this->entityManager->persist($prnCounter);
        }
        $this->entityManager->flush();
    }
}
