<?php

namespace App\Controller\Website;

use App\Entity\Printers;
use App\Managers\PrinterCounterManager;
use App\Repository\PrintersRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PrintersController extends AbstractController
{
    private PrintersRepository $printersRepository;
    private EntityManagerInterface $entityManager;
    private PrinterCounterManager $printerCounterManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        PrinterCounterManager $printerCounterManager
    ) {
        $this->entityManager = $entityManager;
        $this->printersRepository = $this->entityManager->getRepository(Printers::class);
        $this->printerCounterManager = $printerCounterManager;
    }

    /**
     * @Route("/printers/reporting", name="app_printers_active_reported")
     * @throws Exception
     */
    public function activeReported(): Response
    {
        $printers = $this->printersRepository->findActiveAndReported();

        $counters = $this->printerCounterManager->mapCountersForMonth($printers);


        return $this->render('printers/printer_table_report.html.twig', [
            'controller_name' => 'PrintersController',
            'printers' => $printers,
            'counters' => $counters
        ]);
    }


    /**
     * @Route("/printers/notreporting", name="app_printers_not_reported")
     * @throws Exception
     */
    public function activeNotReported(): Response
    {
        $printers = $this->printersRepository->findActiveAndNotReported();
        $counters = $this->printerCounterManager->mapCountersForMonth($printers);



        return $this->render('printers/printer_table_report.html.twig', [
            'controller_name' => 'PrintersController',
            'printers' => $printers,
            'counters' => $counters
        ]);
    }

    /**
     * @Route("/printers/faulty", name="app_printers_faulty")
     * @throws Exception
     */
    public function activeFaulty(): Response
    {
        $printers = $this->printersRepository->findActiveAndFaulty();
        $counters = $this->printerCounterManager->mapCountersForMonth($printers);

        return $this->render('printers/printer_table_report.html.twig', [
            'controller_name' => 'PrintersController',
            'printers' => $printers,
            'counters' => $counters
        ]);
    }

    /**
     * @Route("/printers/inactive", name="app_printers_inactive")
     * @throws Exception
     */
    public function inactivePrinters(): Response
    {
        $printers = $this->printersRepository->findInactive();
        $counters = $this->printerCounterManager->mapCountersForMonth($printers);

        return $this->render('printers/printer_table_report.html.twig', [
            'controller_name' => 'PrintersController',
            'printers' => $printers,
            'counters' => $counters
        ]);
    }
}
