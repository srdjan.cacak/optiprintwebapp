<?php

namespace App\Controller\Website;

use App\Entity\Printers;
use App\Managers\PrinterCounterManager;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use Sulu\Bundle\ContactBundle\Entity\AccountRepositoryInterface;
use Sulu\Bundle\WebsiteBundle\Controller\DefaultController;
use Sulu\Component\Content\Compat\StructureInterface;

class HomepageController extends DefaultController
{

    private ObjectRepository $printersRepository;
    private AccountRepositoryInterface $accountRepository;
    private EntityManagerInterface $entityManager;
    private $printersManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        AccountRepositoryInterface $accountRepository,
        PrinterCounterManager $printersManager
    ){
        $this->accountRepository = $accountRepository;
        $this->entityManager = $entityManager;
        $this->printersRepository = $this->entityManager->getRepository(Printers::class);
        $this->printersManager = $printersManager;
    }

    /**
     * @param $attributes
     * @param StructureInterface|null $structure
     * @param $preview
     * @return array
     * @throws Exception
     */
    protected function getAttributes($attributes, StructureInterface $structure = null, $preview = false): array
    {

        $printers = $this->printersRepository->matching(
            Criteria::create()->where(
                Criteria::expr()->eq('status', 1)
            )
        );

        $attributes = parent::getAttributes($attributes, $structure, $preview);

        $counters = $this->printersManager->mapCountersForMonth($printers);

        $attributes['printers'] = $printers;
        $attributes['counters'] = $counters;
        $attributes['accounts'] = $this
            ->printersManager
            ->getActiveAccounts($printers);

        return $attributes;
    }
}