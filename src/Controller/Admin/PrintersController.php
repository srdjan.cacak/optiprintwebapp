<?php

namespace App\Controller\Admin;

use App\Entity\Printers;
use App\Entity\PrinterTypes;
use App\Managers\PrinterCounterManager;
use App\Repository\PrintersRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use Sulu\Bundle\ContactBundle\Entity\AccountAddress;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\Exception\EntityNotFoundException;
use Sulu\Component\Rest\RestHelperInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Sulu\Component\Rest\ListBuilder\PaginatedRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Psr\Log\LoggerInterface;

/**
 * @RouteResource("printers")
 */
class PrintersController extends AbstractRestController implements ClassResourceInterface
{
    private ViewHandlerInterface $viewHandler;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private DoctrineListBuilderFactoryInterface $listBuilderFactory;
    private RestHelperInterface $restHelper;
    private PrintersRepository $repository;
    private EntityManagerInterface $entityManager;
    private PrinterCounterManager $counterManager;
    private LoggerInterface $logger;

    /**
     * @param ViewHandlerInterface $viewHandler
     * @param FieldDescriptorFactoryInterface $fieldDescriptorFactory
     * @param DoctrineListBuilderFactoryInterface $listBuilderFactory
     * @param RestHelperInterface $restHelper
     * @param PrintersRepository $repository
     * @param EntityManagerInterface $entityManager
     * @param PrinterCounterManager $counterManager
     */
    public function __construct(
        ViewHandlerInterface $viewHandler,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        DoctrineListBuilderFactoryInterface $listBuilderFactory,
        RestHelperInterface $restHelper,
        PrintersRepository $repository,
        EntityManagerInterface $entityManager,
        PrinterCounterManager $counterManager,
        LoggerInterface $logger
    ) {
        $this->viewHandler = $viewHandler;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->listBuilderFactory = $listBuilderFactory;
        $this->restHelper = $restHelper;
        $this->repository = $repository;
        $this->entityManager = $entityManager;
        $this->counterManager = $counterManager;
        $this->logger = $logger;

        parent::__construct($viewHandler);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function cgetAction(Request $request): Response
    {
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Printers::RESOURCE_KEY);
        $listBuilder = $this->listBuilderFactory->create(Printers::class);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);

        if($request->headers->has('X-LOCATION'))
        {
            $account = $this->entityManager->getRepository(AccountAddress::class)->find($request->headers->get('X-LOCATION'));

            $printerIds = array_map(function(?Printers $data) {
                return $data->getId();
            }, $this->repository->findBy(['organizationLocations'=>$account])
            );

        }
        if(
            $request->headers->has('X-LOCATION') &&
            $request->headers->has('X-AUTH-USER') &&
            $request->headers->has('X-AUTH-TOKEN') &&
            $request->headers->has('X-API-REQUEST')
        ) {
            $printerIds[] = 0;
            $listBuilder->in($fieldDescriptors['id'], $printerIds);
        }


        $listRepresentation = new PaginatedRepresentation(
            $listBuilder->execute(),
            Printers::RESOURCE_KEY,
            $listBuilder->getCurrentPage(),
            $request->headers->has('X-API-REQUEST') ? 10000 : 10,
            $listBuilder->count()
        );

        return $this->viewHandler->handle(View::create($listRepresentation));
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function getAction(int $id, Request $request): Response
    {
        $entity = $this->repository->find($id);

        return $this->renderView($entity);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function postAction(Request $request): Response
    {
        $responseText = [];

        [$printerData, $versionData] = $this
            ->counterManager
            ->parseRequest(
                $request->getContent(),
                $request->request->all()
            );


        if(!in_array('formRequest', array_keys($request->request->all()))) {
            $this->counterManager->mapApiCall($printerData, $versionData);
            return new Response(json_encode($responseText), 200);
        }


        $entity = $this->mapData(
            $request->request->all(),
            $this->create()
        );

        return $this->renderView($entity);
    }

    /**
     * @throws EntityNotFoundException
     */
    public function putAction(int $id, Request $request): Response
    {
        if(!$entity = $this->repository->find($id))
            throw new EntityNotFoundException('Printers', $id);

        $entity = $this->mapData(
            $request->request->all(),
            $entity
        );

        return $this->renderView($entity);
    }

    /**
     * @param array $data
     * @param Printers $entity
     * @return Printers
     */
    private function mapData(array $data, Printers $entity): Printers
    {
        if(empty($data['printerType']))
            return

        $entity->setPrinterType($this->entityManager->getRepository(PrinterTypes::class)->find($data['printerType'] ?? 1));
        $entity->setSerialNumber($data['serialNumber']);
        $entity->setNotes($data['notes'] ?? '');
        $entity->setOrganizationLocations($this->entityManager->getRepository(AccountAddress::class)->find($data['organizationLocations']));
        $entity->setIpAddress($data['ipAddress']);
        $entity->setPrinterName($data['printerName'] ?? 'Optiprint Generic Printer');
        $entity->setStatus(1);
        $entity->setPrinterLimit($data['printerLimit'] ?? 0);
        $entity->setPrinterLimitPercentage($data['printerLimitPercentage'] ?? 0);

        return $entity;
    }

    /**
     * @return Printers
     */
    private function create(): Printers
    {
        return new Printers();
    }

    /**
     * @param Printers $entity
     * @return Printers
     */
    public function save(Printers $entity): Printers
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }

    /**
     * @param $entity
     * @return Response
     */
    private function renderView($entity): Response
    {
        if($entity instanceof Printers)
            $entity = $this->save($entity);

        return $this->viewHandler->handle(View::create($entity));
    }
}