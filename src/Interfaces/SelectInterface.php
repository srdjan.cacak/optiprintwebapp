<?php

namespace App\Interfaces;

interface SelectInterface
{

    /**
     * @return array
     */
    public function getValues(): array;

    /**
     * @return array|int
     */
    public function getDefaultValue(?bool $multiSelect);

}