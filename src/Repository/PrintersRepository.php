<?php

namespace App\Repository;

use App\Entity\PrinterCounters;
use App\Entity\Printers;
use App\Entity\PrinterTypes;
use App\Helpers\EntityHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use PHPUnit\Util\Printer;
use Sulu\Bundle\ContactBundle\Entity\AccountAddress;

/**
 * @extends ServiceEntityRepository<Printers>
 *
 * @method Printers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Printers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Printers[]    findAll()
 * @method Printers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrintersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Printers::class);
    }

    public function add(Printers $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Printers $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findActiveAndReported()
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.printerCounters', 'pc')
            ->where('p.status = :printerStatus')
            ->andWhere('pc.date >= :ltDate')
            ->setParameter('printerStatus', Printers::PRINTER_ACTIVE)
            ->setParameter('ltDate', date('Y-m-d', strtotime('-5 days')))
            ->getQuery()
            ->getResult();
    }

    /**
     * @return ?array
     */
    public function findActiveAndNotReported(): ?array
    {
        $sql = 'select p.* from printers p
                inner join
                    (select printer_id, max(date) date from printer_counters group by 1) pc
                        on pc.printer_id = p.id
                where 
                    pc.date < :reportDate AND
                    p.status = :printerStatus AND
                    p.organization_locations_id IS NOT NULL';

        $rsm = EntityHelper::generatePrinterRsm();


        $idsArray = array_map(fn($printer) => $printer['id'], $this->getEntityManager()
            ->createNativeQuery(
                $sql,
                $rsm
            )
            ->setParameter('reportDate', date('Y-m-d', strtotime('-5 days')))
            ->setParameter('printerStatus', Printers::PRINTER_ACTIVE)
            ->disableResultCache()
            ->getArrayResult()
        );

        return $this->findBy(['id'=>$idsArray]);
    }

    public function findActiveAndFaulty()
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.printerCounters', 'pc')
            ->where('p.status = :printerStatus')
            ->andWhere('pc.date IS NULL')
            ->setParameter('printerStatus', Printers::PRINTER_ACTIVE)
            ->getQuery()
            ->getResult();
    }

    public function findInactive()
    {
        return $this->createQueryBuilder('p')
            ->where('p.status = :printerStatus')
            ->setParameter('printerStatus', Printers::PRINTER_INACTIVE)
            ->getQuery()
            ->getResult();
    }

}
