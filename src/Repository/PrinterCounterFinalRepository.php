<?php

namespace App\Repository;

use App\Entity\PrinterCounterFinal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PrinterCounterFinal>
 *
 * @method PrinterCounterFinal|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrinterCounterFinal|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrinterCounterFinal[]    findAll()
 * @method PrinterCounterFinal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrinterCounterFinalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrinterCounterFinal::class);
    }

    public function add(PrinterCounterFinal $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(PrinterCounterFinal $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return PrinterCounterFinal[] Returns an array of PrinterCounterFinal objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PrinterCounterFinal
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
