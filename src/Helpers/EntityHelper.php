<?php

namespace App\Helpers;

use App\Entity\Printers;
use Doctrine\ORM\Query\ResultSetMapping;

class EntityHelper
{

    /**
     * @return ResultSetMapping
     */
    public static function generatePrinterRsm(): ResultSetMapping
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Printers::class, 'p');
        $rsm->addFieldResult('p', 'id', 'id');
//        $rsm->addFieldResult('p', 'printer_name', 'printerName');
//        $rsm->addFieldResult('p', 'serial_number', 'serialNumber');
//        $rsm->addFieldResult('p', 'notes', 'notes');
//        $rsm->addFieldResult('p', 'ip_address', 'ipAddress');
//        $rsm->addFieldResult('p', 'printer_counters', 'printerCounters');
//        $rsm->addFieldResult('p', 'status', 'status');
//        $rsm->addFieldResult('p', 'app_version', 'appVersion');
//        $rsm->addFieldResult('p', 'printer_reports', 'printerReports');
//        $rsm->addFieldResult('p', 'printer_limit', 'printerLimit');
//        $rsm->addFieldResult('p', 'printer_limit_percentage', 'printerLimitPercentage');


        return $rsm;
    }

}