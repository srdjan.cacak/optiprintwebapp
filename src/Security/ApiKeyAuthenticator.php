<?php

namespace App\Security;

use App\Entity\User;
use Sulu\Component\Security\Authentication\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class ApiKeyAuthenticator extends AbstractAuthenticator
{
    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request): ?bool
    {

        if($request->headers->has('X-API-REQUEST') && $request->headers->get('X-API-REQUEST') == 'vb.net.app')
            return true;

        return false;
    }

    public function authenticate(Request $request): Passport
    {
        $apiToken = $request->headers->get('X-AUTH-TOKEN');
        $userIdentifier = $request->headers->get('X-AUTH-USER');

        $message = [];
        if (null === $apiToken)
            $message[] = 'No API token provided';

        if (null === $userIdentifier)
            $message[] = 'No User Identifier provided';

        if (count($message))
            throw new CustomUserMessageAuthenticationException(implode(", ", $message));


        $passport = new Passport(new UserBadge($userIdentifier),
            new \Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials(
            function ($credentials, User $user) {
                return $user->getApiToken() === $credentials;
            },

            // The custom credentials
            $apiToken
        ));

        return $passport;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}