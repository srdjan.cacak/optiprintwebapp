<?php

namespace App\Managers;

use App\Entity\PrinterCounters;
use App\Entity\Printers;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class PrinterCounterManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $getContent
     * @return array
     */
    public function parseRequest($getContent, array $requestData): array
    {
        if(empty($getContent))
            return true;

        $requestBody = json_decode($getContent, true);

        return [
            $requestBody['printers'] ?? null,
            $requestBody['version'] ?? null
        ];
    }

    /**
     * @return $this
     */
    public function mapApiCall(?array $printerData, ?string $versionData): self
    {
        foreach($printerData as $id => $counter)
        {
            $printerObject = $this
                ->entityManager
                ->getRepository(
                    Printers::class
                )->find($id);

            $po = ($printerObject) ? $printerObject->getId() : 0;

            $prnCounter = $this
                ->entityManager
                ->getRepository(
                    PrinterCounters::class
                )->findOneBy(
                    [
                        'printer'=>$printerObject,
                        'date'=>new DateTime()
                    ]
                ) ?? new PrinterCounters();

            if(!$prnCounter->getId()) {
                $prnCounter->setPrinter($printerObject);
                $prnCounter->setSerialNumber($printerObject->getSerialNumber());
                $prnCounter->setOrganization($printerObject->getOrganizationLocations()->getAccount());
            }

            $prnCounter->setDate(new DateTime());
            $prnCounter->setDatetime(new DateTime());
            $prnCounter->setCounter($counter);

            if($versionData) {
                $printerObject->setAppVersion($versionData);
                $this->entityManager->persist($printerObject);
            }

            $this->entityManager->persist($prnCounter);
        }

        $this->entityManager->flush();

        return $this;
    }

    /**
     * @param $printers
     * @return ArrayCollection
     */
    public function getActiveAccounts($printers): ArrayCollection
    {
        $accounts = new ArrayCollection();
        /** @var Printers $printer */
        foreach($printers as $printer)
        {
            $account = $printer->getOrganizationLocations()->getAccount();
            if(!$accounts->contains($account))
                $accounts->add($account);
        }

        return $accounts;
    }

    /**
     * @param $printers
     * @return array|int[]
     * @throws Exception
     */
    public function mapCountersForMonth($printers): array
    {
        $presentCounters = $lastMonthCounters = 0;

        if($printers instanceof Printers)
            $printers = [$printers];

        foreach($printers as $printer)
        {
            /** @var Printers $printer */
            $presentCalculate = $this
                ->printerCalculate(
                    $printer->getPresentMonthCounters()
                );
            if($presentCalculate)
                $presentCounters = $presentCounters + $presentCalculate;


            $lastMonthCalculate = $this
                ->printerCalculate(
                    $printer->getLastMonthCounters()
                );
            if($lastMonthCalculate)
                $lastMonthCounters = $lastMonthCounters + $lastMonthCalculate;
        }

        return [
            'presentMonth' => $presentCounters,
            'lastMonth' => $lastMonthCounters
        ];
    }

    /**
     * @param ArrayCollection|null $printer
     * @return int
     */
    private function printerCalculate(?ArrayCollection $printer): int
    {
        if(!$printer || !$printer->count())
            return 0;

        $printerCounter = [];
        /** @var PrinterCounters $counter */
        foreach($printer as $counter)
            $printerCounter[$counter->getDate()->format('Ymd')] = $counter->getCounter();

        ksort($printerCounter);

        if(end($printerCounter) > reset($printerCounter))
            return end($printerCounter) - reset($printerCounter);

        return 0;
    }

}