<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PrinterTypesRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Exclude;

/**
 * @ORM\Entity(repositoryClass=PrinterTypesRepository::class)
 */
class PrinterTypes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeName;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $snmpPid;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $namePid;

    /**
     * @ORM\OneToMany(targetEntity="Printers", mappedBy="printerType")
     * @Exclude
     */
    private $printers;

    public function __construct()
    {
        $this->printers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeName(): ?string
    {
        return $this->typeName;
    }

    public function setTypeName(string $typeName): self
    {
        $this->typeName = $typeName;

        return $this;
    }

    public function getSnmpPid(): ?string
    {
        return $this->snmpPid;
    }

    public function setSnmpPid(string $snmpPid): self
    {
        $this->snmpPid = $snmpPid;

        return $this;
    }

    /**
     * @return Collection<int, Printers>
     */
    public function getPrinters(): Collection
    {
        return $this->printers;
    }

    public function addPrinter(Printers $printer): self
    {
        if (!$this->printers->contains($printer)) {
            $this->printers[] = $printer;
            $printer->setPrinterType($this);
        }

        return $this;
    }

    public function removePrinter(Printers $printer): self
    {
        if ($this->printers->removeElement($printer)) {
            // set the owning side to null (unless already changed)
            if ($printer->getPrinterType() === $this) {
                $printer->setPrinterType(null);
            }
        }

        return $this;
    }

    public function getNamePid(): ?string
    {
        return $this->namePid;
    }

    public function setNamePid(string $namePid): self
    {
        $this->namePid = $namePid;

        return $this;
    }
}
