<?php

namespace App\Entity;

use App\Repository\PrinterCounterFinalRepository;
use Doctrine\ORM\Mapping as ORM;
use Sulu\Component\Persistence\Model\AuditableInterface;
use Sulu\Component\Persistence\Model\AuditableTrait;

/**
 * @ORM\Entity(repositoryClass=PrinterCounterFinalRepository::class)
 */
class PrinterCounterFinal implements AuditableInterface
{

    use AuditableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Printers::class, inversedBy="printerReports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $printers;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="integer")
     */
    private $month;

    /**
     * @ORM\Column(type="integer")
     */
    private $startCounter;

    /**
     * @ORM\Column(type="integer")
     */
    private $endCounter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrinters(): ?Printers
    {
        return $this->printers;
    }

    public function setPrinters(?Printers $printers): self
    {
        $this->printers = $printers;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getMonth(): ?int
    {
        return $this->month;
    }

    public function setMonth(int $month): self
    {
        $this->month = $month;

        return $this;
    }

    public function getStartCounter(): ?int
    {
        return $this->startCounter;
    }

    public function setStartCounter(int $startCounter): self
    {
        $this->startCounter = $startCounter;

        return $this;
    }

    public function getEndCounter(): ?int
    {
        return $this->endCounter;
    }

    public function setEndCounter(int $endCounter): self
    {
        $this->endCounter = $endCounter;

        return $this;
    }
}
