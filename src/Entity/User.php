<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sulu\Bundle\SecurityBundle\Entity\User as SuluUser;

/**
 * The following annotations are required for replacing the table of the extended entity:
 *
 * @ORM\Table(name="se_users")
 * @ORM\Entity
 */
class User extends SuluUser
{
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $apiToken;

    /**
     * @return string|null
     */
    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    /**
     * @param string $apiToken
     * @return void
     */
    public function setApiToken(string $apiToken): void
    {
        $this->apiToken = $apiToken;
    }

}