<?php

namespace App\Entity;

use App\Repository\PrinterCountersRepository;
use Doctrine\ORM\Mapping as ORM;
use Sulu\Bundle\ContactBundle\Entity\AccountInterface;
use JMS\Serializer\Annotation\Exclude;

/**
 * @ORM\Entity(repositoryClass=PrinterCountersRepository::class)
 */
class PrinterCounters
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Printers::class, inversedBy="printerCounters")
     * @ORM\JoinColumn(nullable=false)
     * @Exclude
     */
    private $printer;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $counter;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $serialNumber;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="printerCounters")
     * @Exclude
     */
    private $organization;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrinter(): ?Printers
    {
        return $this->printer;
    }

    public function setPrinter(?Printers $printer): self
    {
        $this->printer = $printer;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getCounter(): ?int
    {
        return $this->counter;
    }

    public function setCounter(int $counter): self
    {
        $this->counter = $counter;

        return $this;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(?string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getOrganization(): ?Account
    {
        return $this->organization;
    }

    public function setOrganization(?AccountInterface $organization): self
    {
        $this->organization = $organization;

        return $this;
    }
}
