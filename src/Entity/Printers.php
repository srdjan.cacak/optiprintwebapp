<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\PrintersRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Sulu\Bundle\ContactBundle\Entity\AccountAddress;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Exclude;

/**
 * @ORM\Entity(repositoryClass=PrintersRepository::class)
 */
class Printers
{
    const RESOURCE_KEY = 'printers';

    const PRINTER_ACTIVE = 1;
    const PRINTER_INACTIVE = 0;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $printerName;

    /**
     * @ORM\ManyToOne(targetEntity="PrinterTypes", inversedBy="printers")
     * @Exclude
     */
    private $printerType;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $serialNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;
    
    /**
     * @ORM\ManyToOne(targetEntity=AccountAddress::class)
     * @ORM\JoinColumn(nullable=true)
     * @Exclude
     */
    private $organizationLocations;

    /**
     * @ORM\Column(type="string")
     */
    private $ipAddress;

    /**
     * @ORM\OneToMany(targetEntity=PrinterCounters::class, mappedBy="printer", orphanRemoval=true)
     * @Exclude
     */
    private $printerCounters;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    private EntityManagerInterface $entityManager;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $appVersion;

    /**
     * @ORM\OneToMany(targetEntity=PrinterCounterFinal::class, mappedBy="printers", orphanRemoval=true)
     */
    private $printerReports;

    /**
     * @ORM\Column(type="integer")
     */
    private $printerLimit;

    /**
     * @ORM\Column(type="integer")
     */
    private $printerLimitPercentage;

    /**
     *
     */
    public function __construct()
    {
        $this->printerCounters = new ArrayCollection();
        $this->printerReports = new ArrayCollection();
        $this->printerLimit =
            $this->printerLimitPercentage = 0;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrinterType(): ?PrinterTypes
    {
        return $this->printerType;
    }

    public function setPrinterType(?PrinterTypes $printerType): self
    {
        $this->printerType = $printerType;

        return $this;
    }


    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("pid")
     */
    public function getPid(): ?string
    {
        if(!$this->getPrinterType())
            return null;

        return $this->getPrinterType()->getSnmpPid();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("printerType")
     */
    public function getPrinterTypeId(): ?int
    {
        if(!$this->getPrinterType())
            return null;

        return $this->getPrinterType()->getId();
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    public function getOrganizationLocations(): ?AccountAddress
    {
        return $this->organizationLocations;
    }

    public function setOrganizationLocations(?AccountAddress $organizationLocations): self
    {
        $this->organizationLocations = $organizationLocations;

        return $this;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("organizationLocations")
     */
    public function getOrganizationLocationsId()
    {
        if(!$this->getOrganizationLocations())
            return null;

        return $this->getOrganizationLocations()->getId();
    }

    public function getPrinterName(): ?string
    {
        return $this->printerName;
    }

    public function setPrinterName(string $printerName): self
    {
        $this->printerName = $printerName;

        return $this;
    }

    /**
     * @return Collection<int, PrinterCounters>
     */
    public function getPrinterCounters(): Collection
    {
        return $this->printerCounters;
    }

    public function addPrinterCounter(PrinterCounters $printerCounter): self
    {
        if (!$this->printerCounters->contains($printerCounter)) {
            $this->printerCounters[] = $printerCounter;
            $printerCounter->setPrinter($this);
        }

        return $this;
    }

    public function removePrinterCounter(PrinterCounters $printerCounter): self
    {
        if ($this->printerCounters->removeElement($printerCounter)) {
            // set the owning side to null (unless already changed)
            if ($printerCounter->getPrinter() === $this) {
                $printerCounter->setPrinter(null);
            }
        }

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public function isStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("printerCounters")
     */
    public function getPrinterCounter()
    {
        if(!$this->getPrinterCounters() || !$this->getPrinterCounters()->last())
            return null;

        return $this->getPrinterCounters()->last()->getCounter();
    }

    /**
     * @throws \Exception
     */
    public function getPresentMonthCounters()
    {
        if(!$this->getPrinterCounters())
            return null;

        return $this->printerCounters->matching(
            Criteria::create()->where(
                Criteria::expr()->gte('date', new DateTime(date('Y-m-t', strtotime("-1 months"))))
            )
        ) ?? null;
    }

    /**
     * @throws \Exception
     */
    public function getLastMonthCounters()
    {
        if(!$this->getPrinterCounters())
            return null;

        return $this->printerCounters->matching(
            Criteria::create()->where(
                Criteria::expr()->gte('date', new DateTime(date('Y-m-t', strtotime("-2 months"))))
            )->andWhere(
                Criteria::expr()->lt('date', new DateTime(date('Y-m-01')))
            )
        );
    }

    public function getAppVersion(): ?string
    {
        return $this->appVersion;
    }

    public function setAppVersion(?string $appVersion): self
    {
        $this->appVersion = $appVersion;

        return $this;
    }

    /**
     * @return Collection<int, PrinterCounterFinal>
     */
    public function getPrinterReports(): Collection
    {
        return $this->printerReports;
    }

    public function addPrinterReport(PrinterCounterFinal $printerReport): self
    {
        if (!$this->printerReports->contains($printerReport)) {
            $this->printerReports[] = $printerReport;
            $printerReport->setPrinters($this);
        }

        return $this;
    }

    public function removePrinterReport(PrinterCounterFinal $printerReport): self
    {
        if ($this->printerReports->removeElement($printerReport)) {
            // set the owning side to null (unless already changed)
            if ($printerReport->getPrinters() === $this) {
                $printerReport->setPrinters(null);
            }
        }

        return $this;
    }

    public function getPrinterLimit(): ?int
    {
        return $this->printerLimit;
    }

    public function setPrinterLimit(int $printerLimit): self
    {
        $this->printerLimit = $printerLimit;

        return $this;
    }

    public function getPrinterLimitPercentage(): ?int
    {
        return $this->printerLimitPercentage;
    }

    public function setPrinterLimitPercentage(int $printerLimitPercentage): self
    {
        $this->printerLimitPercentage = $printerLimitPercentage;

        return $this;
    }

}
