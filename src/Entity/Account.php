<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Sulu\Bundle\ContactBundle\Entity\Account as SuluAccount;

/**
 * @ORM\Table(name="co_accounts")
 * @ORM\Entity
 */
class Account extends SuluAccount
{

    /**
     * @ORM\OneToMany(targetEntity=PrinterCounters::class, mappedBy="organization")
     */
    private $printerCounters;

    public function __construct()
    {
        parent::__construct();
        $this->printerCounters = new ArrayCollection();
    }

    /**
     * @return Collection<int, PrinterCounters>
     */
    public function getPrinterCounters(): Collection
    {
        return $this->printerCounters;
    }

    public function addPrinterCounter(PrinterCounters $printerCounter): self
    {
        if (!$this->printerCounters->contains($printerCounter)) {
            $this->printerCounters[] = $printerCounter;
            $printerCounter->setOrganization($this);
        }

        return $this;
    }

    public function removePrinterCounter(PrinterCounters $printerCounter): self
    {
        if ($this->printerCounters->removeElement($printerCounter)) {
            // set the owning side to null (unless already changed)
            if ($printerCounter->getOrganization() === $this) {
                $printerCounter->setOrganization(null);
            }
        }

        return $this;
    }

}