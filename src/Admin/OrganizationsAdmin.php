<?php

namespace App\Admin;

use Sulu\Bundle\ContactBundle\Admin\ContactAdmin as SuluOrganizationAdmin;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Security\Authorization\SecurityCheckerInterface;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class OrganizationsAdmin extends SuluOrganizationAdmin
{


    const ORGANIZATION_FORM_KEY = 'organization_form';
    const LOCATIONS_FORM_KEY    = 'locations_form';


    /**
     * @return int
     */
    public static function getPriority(): int
    {
        return SuluOrganizationAdmin::getPriority() - 1;
    }

    public function __construct(
        ViewBuilderFactoryInterface $viewBuilderFactory,
        SecurityCheckerInterface $securityChecker,
        ManagerRegistry $managerRegistry,
        WebspaceManagerInterface $webspaceManager
    )
    {
        parent::__construct($viewBuilderFactory, $securityChecker, $managerRegistry);
        $this->viewBuilderFactory = $viewBuilderFactory;
        $this->securityChecker = $securityChecker;
        $this->webspaceManager = $webspaceManager;
    }
}