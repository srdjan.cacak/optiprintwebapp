<?php

namespace App\Admin;

use Sulu\Bundle\ContactBundle\Admin\ContactAdmin as SuluOrganizationAdmin;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Security\Authorization\SecurityCheckerInterface;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Doctrine\Persistence\ManagerRegistry;
use Sulu\Component\Security\Authorization\PermissionTypes;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use App\Entity\Printers;


class PrintersAdmin extends Admin
{
    
    const PRINTERS_LIST_VIEW = 'app.printer_list';
    const PRINTERS_FORM_KEY = 'printer_details';
    const PRINTERS_ADD_FORM_VIEW = 'app.printers_add_form';
    const PRINTERS_EDIT_FORM_VIEW = 'app.printers_edit_form';
    const PRINTERS_SECURITY_CONTEXT = 'printers';


    private ViewBuilderFactoryInterface $viewBuilderFactory;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory)
    {
        $this->viewBuilderFactory = $viewBuilderFactory;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        $printersNavigationItem = new NavigationItem('app.printers');
        $printersNavigationItem->setView(static::PRINTERS_LIST_VIEW);
        $printersNavigationItem->setIcon('su-paint');
        $printersNavigationItem->setPosition(30);

        $navigationItemCollection->add($printersNavigationItem);
    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        $listView = $this->viewBuilderFactory->createFormOverlayListViewBuilder(static::PRINTERS_LIST_VIEW, '/printers')
            ->setResourceKey(Printers::RESOURCE_KEY)
            ->setListKey('printers')
            ->addListAdapters(['table'])
            ->setFormKey('printer_details')
            ->addToolbarActions([new ToolbarAction('sulu_admin.add')]);

        $viewCollection->add($listView);

        $addFormView = $this->viewBuilderFactory->createResourceTabViewBuilder(static::PRINTERS_ADD_FORM_VIEW, '/printers/add')
            ->setResourceKey(Printers::RESOURCE_KEY)
            ->setBackView(static::PRINTERS_LIST_VIEW);

        $viewCollection->add($addFormView);


        $addDetailsFormView = $this->viewBuilderFactory->createFormViewBuilder(static::PRINTERS_ADD_FORM_VIEW . '.details', '/details')
            ->setResourceKey(Printers::RESOURCE_KEY)
            ->setFormKey(static::PRINTERS_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->setEditView(static::PRINTERS_EDIT_FORM_VIEW)
            ->addToolbarActions([new ToolbarAction('sulu_admin.save')])
            ->setParent(static::PRINTERS_ADD_FORM_VIEW);

        $viewCollection->add($addDetailsFormView);

        $editFormView = $this->viewBuilderFactory->createResourceTabViewBuilder(static::PRINTERS_EDIT_FORM_VIEW, '/printers/:id')
            ->setResourceKey(Printers::RESOURCE_KEY)
            ->setBackView(static::PRINTERS_LIST_VIEW);

        $viewCollection->add($editFormView);

        $editDetailsFormView = $this->viewBuilderFactory->createFormViewBuilder(static::PRINTERS_EDIT_FORM_VIEW . '.details', '/details')
            ->setResourceKey(Printers::RESOURCE_KEY)
            ->setFormKey(static::PRINTERS_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->addToolbarActions([new ToolbarAction('sulu_admin.save')])
            ->setParent(static::PRINTERS_EDIT_FORM_VIEW);

        $viewCollection->add($editDetailsFormView);
    }


}