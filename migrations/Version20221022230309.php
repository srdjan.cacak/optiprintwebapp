<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221022230309 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE printers DROP FOREIGN KEY FK_C5381DB72A90AA33');
        $this->addSql('ALTER TABLE organization_locations DROP FOREIGN KEY FK_2FD190BC32C8A3DE');
        $this->addSql('DROP TABLE organization_locations');
        $this->addSql('ALTER TABLE printers DROP FOREIGN KEY FK_C5381DB72A90AA33');
        $this->addSql('ALTER TABLE printers ADD CONSTRAINT FK_C5381DB72A90AA33 FOREIGN KEY (organization_locations_id) REFERENCES co_account_addresses (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE organization_locations (id INT AUTO_INCREMENT NOT NULL, organization_id INT DEFAULT NULL, location_name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_2FD190BC32C8A3DE (organization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE organization_locations ADD CONSTRAINT FK_2FD190BC32C8A3DE FOREIGN KEY (organization_id) REFERENCES co_accounts (id)');
        $this->addSql('ALTER TABLE printers DROP FOREIGN KEY FK_C5381DB72A90AA33');
        $this->addSql('ALTER TABLE printers ADD CONSTRAINT FK_C5381DB72A90AA33 FOREIGN KEY (organization_locations_id) REFERENCES organization_locations (id)');
    }
}
