<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220929192110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE organization_locations CHANGE organization_id organization_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE printers ADD organization_locations_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE printers ADD CONSTRAINT FK_C5381DB72A90AA33 FOREIGN KEY (organization_locations_id) REFERENCES organization_locations (id)');
        $this->addSql('CREATE INDEX IDX_C5381DB72A90AA33 ON printers (organization_locations_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE organization_locations CHANGE organization_id organization_id INT NOT NULL');
        $this->addSql('ALTER TABLE printers DROP FOREIGN KEY FK_C5381DB72A90AA33');
        $this->addSql('DROP INDEX IDX_C5381DB72A90AA33 ON printers');
        $this->addSql('ALTER TABLE printers DROP organization_locations_id');
    }
}
