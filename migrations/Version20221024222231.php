<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221024222231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE printer_counters (id INT AUTO_INCREMENT NOT NULL, printer_id INT NOT NULL, date DATE NOT NULL, counter INT NOT NULL, datetime DATETIME NOT NULL, INDEX IDX_3232A5FD46EC494A (printer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE printer_counters ADD CONSTRAINT FK_3232A5FD46EC494A FOREIGN KEY (printer_id) REFERENCES printers (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE printer_counters DROP FOREIGN KEY FK_3232A5FD46EC494A');
        $this->addSql('DROP TABLE printer_counters');
    }
}
