<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230810190954 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE printer_counter_final (id INT AUTO_INCREMENT NOT NULL, printers_id INT NOT NULL, year INT NOT NULL, month INT NOT NULL, start_counter INT NOT NULL, end_counter INT NOT NULL, created DATETIME NOT NULL, changed DATETIME NOT NULL, idUsersCreator INT DEFAULT NULL, idUsersChanger INT DEFAULT NULL, INDEX IDX_9C3581DF713EF9E2 (printers_id), INDEX IDX_9C3581DFDBF11E1D (idUsersCreator), INDEX IDX_9C3581DF30D07CD5 (idUsersChanger), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE printer_counter_final ADD CONSTRAINT FK_9C3581DF713EF9E2 FOREIGN KEY (printers_id) REFERENCES printers (id)');
        $this->addSql('ALTER TABLE printer_counter_final ADD CONSTRAINT FK_9C3581DFDBF11E1D FOREIGN KEY (idUsersCreator) REFERENCES se_users (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE printer_counter_final ADD CONSTRAINT FK_9C3581DF30D07CD5 FOREIGN KEY (idUsersChanger) REFERENCES se_users (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE printer_per_month');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE printer_per_month (id INT AUTO_INCREMENT NOT NULL, printer_id INT NOT NULL, month SMALLINT NOT NULL, counter INT NOT NULL, organization_id INT DEFAULT NULL, serial_number VARCHAR(128) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, counter_one INT DEFAULT 0 NOT NULL, INDEX IDX_3232A5FD32C8A3DE (organization_id), INDEX IDX_3232A5FD46EC494A (printer_id), UNIQUE INDEX printer_id (printer_id, month, organization_id, serial_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE printer_counter_final DROP FOREIGN KEY FK_9C3581DF713EF9E2');
        $this->addSql('ALTER TABLE printer_counter_final DROP FOREIGN KEY FK_9C3581DFDBF11E1D');
        $this->addSql('ALTER TABLE printer_counter_final DROP FOREIGN KEY FK_9C3581DF30D07CD5');
        $this->addSql('DROP TABLE printer_counter_final');
    }
}
