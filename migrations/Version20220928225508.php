<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220928225508 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE printer_types (id INT AUTO_INCREMENT NOT NULL, type_name VARCHAR(255) NOT NULL, snmp_pid VARCHAR(512) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE printers (id INT AUTO_INCREMENT NOT NULL, printer_type_id INT DEFAULT NULL, serial_number VARCHAR(128) NOT NULL, notes LONGTEXT DEFAULT NULL, INDEX IDX_C5381DB7E1CD7692 (printer_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE printers ADD CONSTRAINT FK_C5381DB7E1CD7692 FOREIGN KEY (printer_type_id) REFERENCES printer_types (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE printers DROP FOREIGN KEY FK_C5381DB7E1CD7692');
        $this->addSql('DROP TABLE printer_types');
        $this->addSql('DROP TABLE printers');
    }
}
