<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221101120245 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE printer_counters ADD organization_id INT DEFAULT NULL, ADD serial_number VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE printer_counters ADD CONSTRAINT FK_3232A5FD32C8A3DE FOREIGN KEY (organization_id) REFERENCES co_accounts (id)');
        $this->addSql('CREATE INDEX IDX_3232A5FD32C8A3DE ON printer_counters (organization_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE printer_counters DROP FOREIGN KEY FK_3232A5FD32C8A3DE');
        $this->addSql('DROP INDEX IDX_3232A5FD32C8A3DE ON printer_counters');
        $this->addSql('ALTER TABLE printer_counters DROP organization_id, DROP serial_number');
    }
}
