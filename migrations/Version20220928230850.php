<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220928230850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE organization_locations (id INT AUTO_INCREMENT NOT NULL, organization_id INT NOT NULL, location_name VARCHAR(255) NOT NULL, INDEX IDX_2FD190BC32C8A3DE (organization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE organization_locations ADD CONSTRAINT FK_2FD190BC32C8A3DE FOREIGN KEY (organization_id) REFERENCES co_accounts (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE organization_locations DROP FOREIGN KEY FK_2FD190BC32C8A3DE');
        $this->addSql('DROP TABLE organization_locations');
    }
}
